import { Component, OnInit } from '@angular/core';
import { GithubService } from '../../services/github.service';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models/user';
import { Repos } from '../../models/repos';

@Component({
  selector: 'app-arreglo',
  templateUrl: './arreglo.component.html',
  styleUrls: ['./arreglo.component.css']
})

export class ArregloComponent implements OnInit {
  repos: Observable<Repos[]>;
  constructor(private _githubService: GithubService) { }

  ngOnInit() {
    this.repos = this._githubService.getReposData();
  }

}
