import { Component, OnInit } from '@angular/core';
import { GithubService } from '../../services/github.service';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models/user';

@Component({
  selector: 'app-objeto',
  templateUrl: './objeto.component.html',
  styleUrls: ['./objeto.component.css']
})
export class ObjetoComponent implements OnInit {

  user: User;

  constructor( private _githubService: GithubService) { }

  ngOnInit() {

    // Tienes que usar el 'subscribe' para obtener la data cuando es solo 1 item {}
    this._githubService.getUserData().subscribe((data) => {
      this.user = data;
    });
  }

}
