import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { ArregloComponent } from '../app/components/arreglo/arreglo.component';
import { ObjetoComponent } from '../app/components/objeto/objeto.component';

const routes: Routes = [
  { path: 'arreglo', component: ArregloComponent},
  { path: 'objeto', component: ObjetoComponent},
  { path: '**', pathMatch: 'full', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
