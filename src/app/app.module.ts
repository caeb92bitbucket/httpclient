import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';

import { environment } from '../environments/environment';

// Servicios
import { GithubService } from '../app/services/github.service';
// Componentes
import { ObjetoComponent } from './components/objeto/objeto.component';
import { ArregloComponent } from './components/arreglo/arreglo.component';

@NgModule({
  declarations: [
    AppComponent,
    ObjetoComponent,
    ArregloComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    GithubService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
