import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


import 'rxjs/add/operator/map';
import { User } from '../models/user';
import { Repos } from '../models/repos';

@Injectable()
export class GithubService {

  readonly postJson = '../../assets/data.json';
  readonly ROOT_URL = 'https://api.github.com/users/caeb92';
  readonly REPOS_URL = 'https://api.github.com/users/caeb92/repos';
  constructor(private _httpClient: HttpClient) {
  }

  getUserData(): Observable<User> {
    return this._httpClient.get<User>(this.ROOT_URL);
  }
  getReposData(): Observable<Repos[]> {
    return this._httpClient.get<Repos[]>(this.postJson);
  }
}
