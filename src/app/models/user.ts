export class User {
    id: number;
    name: string;
    login: string;
    avatar_url: string;
    html_url: string;
    location: string;
    constructor(id: number, name: string, login: string, avatar_url: string, html_url: string, location: string) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.avatar_url = avatar_url;
        this.html_url = html_url;
        this.location = location;
    }
}
