export class Repos {
    postId: number;
    id: number;
    name: string;
    email: string;
    body: string;
    img: string;
    constructor(postId: number, id: number, name: string, email: string, body: string, img: string) {
        this.postId = postId;
        this.id = id;
        this.name = name;
        this.email = email;
        this.body = body;
        this.img = img;
    }
}
